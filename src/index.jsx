const { client, xml, jid } = require("@xmpp/client")
const { EventEmitter } = require('events')

class Client extends EventEmitter {

  constructor(options = {}, ...args) {
    // initial settings go here
    super(...args)

    // example of service: "wss://beta.hool.org:443/xmpp-websocket"

    if (!options.service) {
      console.warn("No XMPP service specified. We won't be able to connect!")
    } else {
      this.service = options.service
    }

    if (!options.standard) {
      console.warn("No game standard specified. Using generic Multi-User Gaming, which may include incompatible game hosts")

      // this should be overridden with a game-specific URI
      // like 'http://hool.org/protocol/mug/hool'

      this.standard = 'http://jabber.org/protocol/mug'
    } else {
      this.standard = options.standard
    }

    this.xmpp = null
    this.jid = null

    // if there's no resource specified, we'll generate a random ID
    this.resource = options.resource || Math.random().toString(16).substr(2, 8)
  }

  // following functions are to be overridden
  handleGamePresence(stanza) {
    // inside this, we can assume that the <game/>
    // element is included
    throw 'Not implemented'
  }

  handleGameTurn(stanza) {
    // inside this, we can assume that the <turn/>
    // element is included
    throw 'Not implemented'
  }

  // connect to the xmpp server
  async xmppConnect(myJID, password=undefined) {

    // note down the JID somewhere
    this.jid = jid(myJID)

    // TODO: temporarily cache password

    // clear old XMPP object
    if (this.xmpp) {
      await this.xmpp.stop()
    }


    if (!password) {

      // if there's no password, it means
      // it's an anonymous login
      this.xmpp = client({
        service: this.service,
        domain: myJID, // not the parsed one
        resource: this.resource,
      })

    } else {

      // otherwise, it's a normal login
      this.xmpp = client({
        service: this.service,
        domain: this.jid.domain,
        resource: this.resource,
        username: this.jid.local,
        password: password,
      })

    }

    // listen if not listening
    this.listen(this.xmpp)
  }

async listen(connection) {

    const { iqCaller } = connection

    connection.on("error", (err) => {
      let error = {}

      // fetch params
      error.code = err.code

      if (err.code == 'ECONNERROR') {
        error.message = 'Connection error. Please try again'
      } else if (err.condition == 'not-authorized') {
        error.code = 'not-authorized'
        error.message = 'Invalid username or password'
      } else{
        error.message = 'Unknown error'
      }

      this.emit('error', error)
    })

    connection.on("offline", () => {
      console.log("offline")
      this.emit('offline', {})
    })

    connection.on("stanza", async (stanza) => {
      console.log(`We got: ${stanza}`)

      let from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined

      // ignore own messages
      if (
        from &&
        from.local == this.jid.local &&
        from.host == this.jid.host
      ) {
        console.log("ignoring own message")
        return
      }

      // handle presence
      if (stanza.is("presence")) {

        if (stanza.getChildren("game").length) {

          // allow game presences to be overridden
          let e = this.handleGamePresence(stanza)
          if (!!e) {

            // handled; let's send it out
            this.emit(e.event, e)
            return

          } else {

            // not handled; maybe we can try then
            let game = stanza.getChild("game")

            let role
            let affiliation
            let room = jid(stanza.attrs.from).bare().toString()
            let nickname = jid(stanza.attrs.from).resource
            let user = nickname // to be overridden

            if (game.getChildren("item").length) {
              let item = game.getChild("item")

              role = item.attrs.role
              affiliation = item.attrs.affiliation || unknown
              user = item.attrs.jid
            }

            // handle leaving
            if (stanza.attrs.type == 'unavailable') {

              // check if it's permanent (exit)
              if (
                game.getChildren("item").length &&
                game.getChild("item").attrs.affiliation == "none"
              ) {
                this.emit('roomExit', {
                  user: user,
                  nickname: nickname,
                  room: room,
                })
                return
              }

              // if not permanent, it's temporary
              this.emit('roomDisconnect', {
                user: user,
                room: room,
              })
              return
            }

            // handle error
            if (stanza.getChildren("error").length) {
              let error = stanza.getChild("error")

              this.emit('roomJoinError', {
                room: room,
                user: user,
                nickname: nickname,
                type: error.attrs.type || 'unknown',
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
              })
              return
            }

            // okay, since it's none of those, it must be a
	    // normal join!
	    this.emit('roomJoin', {
              room: room,
              user: user,
              role: role,
              affiliation: affiliation,
              nickname: nickname,
            })
            return
          }

        } else if (stanza.attrs.xmlns == 'jabber:client') {

          // handle non-game presences

          if (stanza.attrs.type == 'subscribe') {
            this.emit('roster-subscribe', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'subscribed') {
            this.emit('roster-subscribed', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unsubscribe') {
            this.emit('roster-unsubscribe', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unsubscribed') {
            this.emit('roster-unsubscribed', {
              user: stanza.attrs.from,
            })
          } else if (stanza.attrs.type == 'unavailable') {
            this.emit('contact-offline', {
              user: stanza.attrs.from,
            })
          } else {
            // extract details
            let show
            let status

            if (stanza.getChildren('show').length) {
              show = stanza.getChild('show').text()
            }

            if (stanza.getChildren('status').length) {
              status = stanza.getChild('status').text()
            }

            this.emit('contact-online', {
              user: stanza.attrs.from,
              show: show,
              status: status,
            })
          }

        }
      }

      // after this, ignore non-messages
      if (!stanza.is("message")) return

      if (stanza.is("message")) {

        // handle turns
        if (stanza.getChildren("turn").length) {
          let e = this.handleGameTurn(stanza)
          if (!!e) {
            this.emit(e.event, e)
            return
          }

          // otherwise, we can't do anything anyway
          return
        }

        // handle "start" indication
        if (stanza.getChildren("start").length) {
          let u = jid(stanza.attrs.from)

          // check if there's an error
          if (stanza.getChildren("error").length) {

            // ideally, we'll check the error type, etc.
            // but for now this is enough.

            this.emit('startError', {
              game: `${u.bare()}`,
              user: u.resource,
            })

          } else {

            // normal start message
            this.emit('start', {
              game: `${u.bare()}`,
              user: u.resource,
            })

          }
        }

        // handle fold 
        if (stanza.getChildren("fold").length) {
          let fold = stanza.getChild("fold")
          let u = jid(stanza.attrs.from);
          if (stanza.getChildren("error").length) {
            this.emit("foldError", {
              game: `${u.bare()}`,
              user: u.resource,
            });
          } else {
            this.emit("fold", {
              game: `${u.bare()}`,
              user: u.resource,
              player: fold.attrs.player,
              nickname: jid(stanza.attrs.from).resource
            });
          }
        }

        // handle other game-related messages
        if (stanza.getChildren("game").length) {
          let game = stanza.getChild("game")

          // check that it's compatible; if not we silently drop
          if  (game.attrs.xmlns != 'http://jabber.org/protocol/mug#user') {
            console.warn(`Dropping unrecognised stanza: ${stanza}`)
          }

          // invited
          let invited = game.getChild("invited")

          if (
            invited &&
            invited.attrs.var == this.standard
          ) {
            // TODO: reject malformed stanza

            let reason = invited.getChild("reason") || null
            if (reason) reason = reason.text()

            let role = null
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null
            }

            this.emit('invited', {
              invitor: invited.attrs.from,
              invitee: stanza.attrs.to,
              role: role,
              game: stanza.attrs.from,
              reason: reason,
            })
          }

          // declined invite
          let declined = game.getChild("declined")

          if (declined) {
            // TODO: reject malformed stanza

            let reason = declined.getChild("reason") || null
            if (reason) reason = reason.text()

            let role = null
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null
            }

            this.emit('invite-declined', {
              invitee: stanza.attrs.from,
              invitor: declined.attrs.to,
              game: stanza.attrs.from,
              reason: reason,
              role: role,
            })
          }
        }

        // handle chat and groupchat messages
        if (
          stanza.attrs.type == 'groupchat' &&
          stanza.getChildren("body").length
        ) {
          // break up username
          let sender = stanza.attrs.from.split('/')
          this.emit('groupchat', {
             from: stanza.attrs.from,
             to: stanza.attrs.to,
             user: sender[1],
             game: sender[0],
             id: stanza.attrs.id,
             text: stanza.getChild("body").text(),
           })
        } else if (
          stanza.attrs.type == 'chat' &&
          stanza.getChildren("body").length
        ) {
          this.emit('chat', {
            from: stanza.attrs.from,
            to: stanza.attrs.to,
            id: stanza.attrs.id,
            text: stanza.getChild("body").text(),
            nickname: jid(stanza.attrs.from).resource,
          })
        }
      }
    })

    connection.on("online", async (address) => {
      console.log("...and we're on!")

      // The JID might be different, if we signed
      // in anonymously
      this.jid = this.xmpp.jid

      await connection.send(
        <presence>
          <show>chat</show>
          <status>Hi, I'm a test user</status>
        </presence>
      )
      console.log("connected to xmpp server")
      this.emit('connected', {
        status: 'ok',
      })
    })

    console.log('All set up. Trying to connect...')
    connection.start().catch(console.error)
  }

  /**
   * Server-level operations
   *
   * Connecting to a server, disconnecting, service discovery.
   */

  async xmppStop() {
    return await this.xmpp.stop()
  }

  async xmppDiscoverServices() {
    // Discover services
    console.log("discovering services")

    let response = await this.xmpp.iqCaller.request(
      <iq type="get">
        <query xmlns="http://jabber.org/protocol/disco#info"/>
      </iq>
    )

    // check if server supports disco#items
    if (response.getChild('query').children.some((e) => {
        return (
          e.name == 'feature' &&
          e.attrs.var == 'http://jabber.org/protocol/disco#items'
        )
      })
    )
    {
      console.log('disco#items supported')
      // get list of items
      response = await this.xmpp.iqCaller.request(
        <iq type="get">
          <query xmlns="http://jabber.org/protocol/disco#items"/>
        </iq>
      )

      var hosts = []

      for await (let service of response.getChild('query').children) {
        // check if it supports our game
        if (service.name == 'item') {

          console.log(`checking: ${service.attrs.jid}`)

          try {

            response = await this.xmpp.iqCaller.request(
              <iq type="get" to={service.attrs.jid}>
                <query xmlns="http://jabber.org/protocol/disco#info"/>
              </iq>
            )

            if (
              response.getChild('query').children.some((feature) => {
                return (
                  feature.name == 'feature' &&
                  feature.attrs.var == this.standard
                )
              })
            ) {
              console.log(`Found game service at ${service.attrs.jid}`)
              hosts.push(service.attrs.jid)
            }

          } catch(e) {
            console.log(`Warning: skipping ${service.attrs.jid}: ${e}`)
          }
        }

      }


      // give up if we haven't found any hosts
      if (hosts.length < 1) {
        console.log("Could not find any hosts for this game :(")
        return
      }

      console.log(`Found hosts: ${hosts}`)

      this.gameService = hosts[0]
      return this.gameService

      } else {
        console.log('disco#items not supported here :(')
        return
      }
  }


  /**
   * Roster operations
   *
   * Friend requests, blocking, presence, sending messages.
   */

  rosterSubscribe(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="subscribe"/>
    )
  }

  rosterSubscribed(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="subscribed"/>
    )
  }

  rosterUnsubscribe(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="unsubscribe"/>
    )
  }

  rosterUnsubscribed(userJID) {
    this.xmpp.send(
      <presence from={this.jid} to={userJID} type="unsubscribed"/>
    )
  }

  async xmppGetRoster() {
    // fetch roster
    console.log("fetching roster")

    let response = await this.xmpp.iqCaller.request(
      <iq type="get">
        <query xmlns="jabber:iq:roster"/>
      </iq>
    )

    if (response.getChildren('query').length) {
      let roster = []

      for (let contact of response.getChild('query').getChildren('item')) {
        roster.push({
          jid: contact.attrs.jid,
          name: contact.attrs.name,
        })
      }

      return roster
    }

    console.log('roster fetched, but no contacts found')
    console.log('make some friends!')
    return []
  }

  /**
   * Game-level operations
   *
   * Listing games, joining or leaving a game, and so on.
   */

  async listGames() {

    if (!this.gameService) {
      await this.xmppDiscoverServices()
    }

    // now, get room list
    var response = await this.xmpp.iqCaller.request(
      <iq type="get" to={this.gameService}>
        <query xmlns="http://jabber.org/protocol/disco#items"/>
      </iq>
    )

    var roomList = [] // temporary list with minimal info

    // save in list
    response.getChild("query").children.forEach((r) => {
        roomList.push({
          jid: r.attrs.jid,
          name: r.attrs.name,
        })
    })


    console.log(`found rooms: ${rooms.map((r)=>{return r.jid})}`)
    return rooms
  }

  async join(gameID, role, nick=undefined) {
    if (!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') == -1) gameID = `${gameID}@${this.gameService}`

    // convert to jid object
    gameID = jid(gameID)

    // set nickname if needed
    if (nick) gameID.resource = nick

    this.xmpp.send(
      <presence from={this.jid} to={gameID}>
        <game var={this.standard}>
          <item role={role}/>
        </game>
      </presence>
    )
  }

  leave(gameID) {
    if(!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') == -1) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <presence
        from={this.jid}
        to={gameID}
        type='unavailable'>
        <game xmlns='http://jabber.org/protocol/mug'>
          <item affiliation='none' role='none' jid={this.jid}/>
        </game>
      </presence>
    )
  }

  invite(gameID, invitee, role=null, reason=null) {
		if (!invitee) throw "Please specify an invitee"
		if (!gameID) throw "Please specify the game to which you are inviting!"

    // auto-set domain
    if (gameID.indexOf('@') == -1) gameID = `${gameID}@${this.gameService}`

		this.xmpp.send(
			<message
				from={this.jid}
				to={gameID}
			>
				<game xmlns='http://jabber.org/protocol/mug#user'>
					<invite to={invitee}>
						{reason ?
							<reason>{reason}</reason>
						:
							''
						}
					</invite>
					{seat ?
						<item role={role}/>
					:
						''
					}
				</game>
			</message>
		)
  }

  decline(gameID, invitor, seat=null, reason=null) {
		if (!invitor) throw "Please specify an invitor"
		if (!gameID) throw "Please specify the game to which you are inviting!"

    // auto-set domain
    if (gameID.indexOf('@') == -1) gameID = `${gameID}@${this.gameService}`

		this.xmpp.send(
			<message
				from={this.jid}
				to={gameID}
			>
				<game xmlns='http://jabber.org/protocol/mug#user'>
					<decline to={invitor}>
						{reason ?
							<reason>{reason}</reason>
						:
							''
						}
					</decline>
					{role ?
						<item role={role}/>
					:
						''
					}
				</game>
			</message>
		)
  }

  // Indicate that we're ready to start the game
  start(gameID) {
    if (!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') < 0) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}>
        <start xmlns='http://jabber.org/protocol/mug#user'/>
      </message>
    )
  }


  /**
   * Chat-level operations
   *
   * Basically for handling literal chat messages
   */

  chat(userJID, text) {
    if (!userJID) throw 'Please specify a user JID!'

    this.xmpp.send(
      <message
        from={this.jid}
        to={userJID}
        type='chat'>
        <body>{text}</body>
      </message>
    )
  }

  groupChat(gameID, text) {
    if (!gameID) throw 'Please specify a gameID'

    // auto-set domain
    if (gameID.indexOf('@') == -1) gameID = `${gameID}@${this.gameService}`

    this.xmpp.send(
      <message
        from={this.jid}
        to={gameID}
        type='groupchat'>
        <body>{text}</body>
      </message>
    )
  }

}

module.exports = {
  Client,
}
